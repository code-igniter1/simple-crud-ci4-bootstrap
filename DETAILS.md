# Simple CRUD CI4 Bootstrap
### Thing todo list:
1. Clone this repository: `git clone https://gitlab.com/code-igniter1/simple-crud-ci4-bootstrap.git`
2. Go inside the folder: `cd simple-crud-ci4-bootstrap`
3. Set Your DB credential on .env file
4. Run this sql script file -> posDB.sql
5. Run `php spark serve`
6. Open your favorite browser: http://localhost:8080/product

### Scren shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Product

![Add New Product](img/add.png "Add New Product")

Edit Product

![Edit Product](img/edit.png "Edit Product")

Delete Product

![Delete Product](img/delete.png "Delete Product")