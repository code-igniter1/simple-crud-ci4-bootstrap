CREATE TABLE category(
                         category_id INT PRIMARY KEY AUTO_INCREMENT,
                         category_name VARCHAR(50)
)ENGINE=INNODB;

CREATE TABLE product
(
    product_id          INT PRIMARY KEY AUTO_INCREMENT,
    product_name        VARCHAR(100),
    product_price       INT,
    product_category_id INT
) ENGINE = INNODB;

INSERT INTO category(category_name)
VALUES ('Books'),('Electronics'),('Fashions');

INSERT INTO product(product_name,product_price,product_category_id)
VALUES ('The 4 Hour Workweek','76000','1'),('CodeIgniter Cheat Sheet','50000','1');